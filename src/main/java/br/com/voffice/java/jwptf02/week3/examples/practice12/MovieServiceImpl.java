package br.com.voffice.java.jwptf02.week3.examples.practice12;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.voffice.java.jwptf02.week3.examples.practice10.InMemoryMovieRepository;
import br.com.voffice.java.jwptf02.week3.examples.practice10.MovieRestController.Movie;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	private InMemoryMovieRepository movieRepository;


	public List<Movie> findAllMovies() {
		return movieRepository.findAll();
	}

}
